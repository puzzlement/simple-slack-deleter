#!/usr/bin/env python3

import datetime

import slack_cleaner2

AGE_TO_DELETE = 42 # 42 days = 6 weeks

# replace with your secret key, or alter the Python to read it in from storage
SECRET_KEY = "xoxp-xxxxxxxxxxx-xxxxxxxxxxx-xxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

def get_end_date():
    return datetime.datetime.now() - datetime.timedelta(days=AGE_TO_DELETE)

def delete_channel_messages(slack, end_date):
    """
    Delete all non-pinned messages in all channels older than AGE_TO_DELETE days
    """
    for message in slack.msgs(channels=slack.channels,
            before=end_date.timestamp()):
        # don't delete pinned messages
        if not message.pinned_to:
            # delete message and its replies
            message.delete(replies=True)

def delete_im_messages(slack, end_date):
    """
    Delete messages in all DMs older than AGE_TO_DELETE days.

    For 1:1 DMs, only attempt to delete messages the owner of the API key
    authored themselves: the Slack API will refuse to delete the other side of
    1:1 conversations.
    """
    for message in slack.msgs(channels=slack.ims, before=end_date.timestamp()):
        # check that the message was authored by the API key's user, then delete
        # otherwise ignore
        if message.user == slack.myself:
            message.delete()
    for message in slack.msgs(channels=slack.mpim, before=end_date.timestamp()):
        message.delete()

def delete_files(slack, end_date):
    """
    Delete all non-pinned files older than AGE_TO_DELETE days
    """
    for file in slack.files(before=end_date.timestamp()):
        # don't delete pinned files
        if not file.pinned_to:
            file.delete()

def main():
    slack = slack_cleaner2.SlackCleaner(SECRET_KEY)
    end_date = get_end_date()
    delete_channel_messages(slack, end_date)
    delete_im_messages(slack, end_date)
    delete_files(slack, end_date)

if __name__ == '__main__':
    main()
